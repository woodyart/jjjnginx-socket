### First

**cd** into project _dir_

### Build containers

```
docker build --rm -t ppphp php/.
docker build --rm -t nnnginx nginx/.
```

### Launch containers

```
docker run -d -v $(pwd)/sock:/var/run/ -v $(pwd)/www/:/var/www/html/ --name phpfpm ppphp
docker run -d -p 8080:80 -v $(pwd)/sock:/var/run/ -v $(pwd)/www/:/var/www/html/ --name nginx3 nnnginx
```

### Test in browser

* http://localhost:8080 - opens phpinfo that means nginx and php-fmp works alltogether
* http://localhost:8080/index.html - test that nginx container is working

### Stop and remove containers

```
docker stop phpfpm && docker rm phpfpm
docker stop nginx3 && docker rm nginx3
```
